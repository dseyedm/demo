# README #
A small cross-platform go-away card in the form of a graphics demo. The packed executable (UPX) is ~72kb, not including the gcc dll.
## [Link to Video Capture](https://drive.google.com/a/uleth.ca/file/d/0BzvoISQz7IZiSkZob05NM2lWblk/view) ##
## [Download (Windows)](https://bitbucket.org/sorajsm/demo/downloads/demo_final_windows.7z) ##
## Content ##
![](https://bitbucket.org/dseyedm/demo/downloads/2015-12-26-174408_1366x768_scrot.png)

![2](https://bitbucket.org/dseyedm/demo/downloads/2015-12-26-174440_1366x768_scrot.png)

![1](https://bitbucket.org/dseyedm/demo/downloads/2015-12-26-174500_1366x768_scrot.png)

## Credits ##
code: Soraj Daniel Seyed-Mahmoud

music: crema lubricante v3 by [tempest (Janne Suni)](https://soundcloud.com/janne-suni)

amiga module player code: hxc by [Jean-François DEL NERO (Jeff) / HxC2001](https://github.com/jfdelnero/HxCModPlayer)

crt shader: [Timothy Lottes](http://timothylottes.blogspot.ca/)

raymarching template: [iq (Íñgo Quílez)](http://www.iquilezles.org/)

math: [linmath.h](https://github.com/datenwolf/linmath.h) by datenwolf

opengl context loading: [gl3w](https://github.com/skaslev/gl3w)

window creation: [glfw](http://www.glfw.org/)