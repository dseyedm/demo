#ifndef UTILS_H
#define UTILS_H

#define PI 3.141592653589793
#define EPS 1e-2

/* 16:9 * 64 */
#define WIDTH 1024
#define HEIGHT 576

#define SIZE(a) (sizeof(a) / sizeof(a[0]))
#define UNUSED(a) (void)(a)

#include <math.h>

inline double rad(double a) { return a * PI / 180.0; }
inline double ndc_y(double wc) { return wc / (0.5 * HEIGHT) - 1.0; }
inline double ndc_x(double wc) { return wc / (0.5 * WIDTH) - 1.0; }
inline double dmod(double t, double m) { return t - m * floor(t / m); }
inline double lerp(double a, double t, double b) { return a + (b - a) * t; }

typedef struct {
	double x, y;
} point_t;

inline point_t lerp_point(point_t* a, double t, point_t* b) { 
	return (point_t){ lerp(a->x, t, b->x), lerp(a->y, t, b->y) };
}

/* cubic bezier */
double cubic_bezier_out(double t, double x0, double y0, double x1, double y1);
#if 0
double cubic_bezier(double t, double x0, double y0, double x1, double y1);
#endif

double ease_out_spring(double t, double b, double c, double d);

#if 1
/* one must free the returned contents */
char* load_file(const char* file);
#endif

#endif
