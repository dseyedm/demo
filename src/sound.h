#ifndef SOUND_H
#define SOUND_H

#include "hxc/hxcmod.h"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#define SAMPLING_FREQ  44100
#define OVERSAMPLE     2     /* 2x oversampling */
#define NUM_CHANNELS   2     /* Stereo */
#define BUFFER_SAMPLES 2048  /* 64k per buffer */
#define NUM_BUFFERS    4     /* 4 buffers (256k) */

extern int tg_playing;
void load_music();

#ifdef OS_WIN
#include <windows.h>
#include <mmsystem.h>

extern HANDLE g_thread_lock;
DWORD WINAPI play_module_thread(LPVOID module);
#endif

#ifdef OS_LIN
#include <alsa/asoundlib.h>
#include <pthread.h>

extern pthread_mutex_t g_thread_lock;
void* play_module_thread(void* thread_data);
#endif

#endif
