#ifndef SCENE_H
#define SCENE_H

#include "graphics.h"

typedef struct {
	double limit;
	void (*render)();
} scene_t;

void test();
void scene0();
void scene1();
void scene2();
void scene3();
void scene4();
void scene5();
void scene6();
void scene7();
void scene8();

#endif
