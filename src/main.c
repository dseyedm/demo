#include "scene.h"
#include "sound.h"

#ifdef OS_LIN
#define min(a, b) (b < a ? b : a)
#endif

/* seconds per 8 beats, per table */
//const double SPT = 4.5883700098039; /* N=102 */
const double SPT = 3.7491569459459; /* N=40 */

int main() {
	if(!setup_graphics()) return 1;

	/* set opengl settings */
	gl(Disable(GL_CULL_FACE));
	gl(CullFace(GL_BACK));
	gl(Enable(GL_BLEND));
	gl(BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
	gl(ClearColor(0.0, 0.0, 0.0, 1.0));
	//gl(PolygonMode(GL_FRONT_AND_BACK, GL_LINE));

	scene_t scenes[] = {
#if 1
		{ 2.0*SPT, scene0 },
		{ 4.0*SPT, scene1 },
		{ 6.0*SPT, scene2 },
		{ 8.5*SPT, scene3 },
		{ 12.5*SPT, scene4 },
		{ 16.5*SPT, scene5 },
		{ 17.5*SPT, scene6 },
		{ 18.5*SPT, scene7 },
		{ 9999.0, scene8 }
#else
		{ 9999.0, scene3 }
#endif
	};

	load_music();

	/* spawn the music thread */
#ifdef OS_LIN
	pthread_t thread_id;
	pthread_mutex_init(&g_thread_lock, NULL);
	pthread_create(&thread_id, NULL, play_module_thread, NULL);
#endif
#ifdef OS_WIN
	g_thread_lock = CreateMutex(NULL, FALSE, NULL);
	DWORD thread_id;
	HANDLE thread_handle = CreateThread(
		NULL,
		0,
		play_module_thread,
		NULL,
		0,
		&thread_id
	);
#endif

	while(!glfwWindowShouldClose(window)) {
		glfwPollEvents();

#if 0
		/* debug */
		static int last_hit = 0;
		if(!last_hit && glfwGetKey(window, GLFW_KEY_SPACE)) {
			printf("%fs %ft\n", glfwGetTime(), glfwGetTime() / SPT);
		}
		last_hit = glfwGetKey(window, GLFW_KEY_SPACE);

		/* reload programs */
		static int last_f5 = 0;
		if(glfwGetKey(window, GLFW_KEY_F5) && !last_f5) {
			destroy_programs();
			create_programs();
			if(!load_programs()) break;
			printf("reloaded programs\n");
		}
		last_f5 = glfwGetKey(window, GLFW_KEY_F5);
#endif
		/* render at half resolution because of the crt shader (thanks timothy lottes!) */
		gl(Viewport(0, 0, WIDTH / 2, HEIGHT / 2));

		/* render scenes to a */
		bind_fbo(fbo_a);
		gl(Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
		for(int i = 0; i < SIZE(scenes); ++i) {
			if(scenes[i].limit > glfwGetTime()) {
				(*scenes[i].render)();
				break;
			}
		}

		/* vignette on a to b */
		bind_program(vignette_program);
		set_uniform2f(vignette_scale, (vec2){ 1.0, 1.0 });
		set_uniform2f(vignette_offset, (vec2){ 0.0, 0.0 });
		/* flash effect */
		{
			double flash = 0.0;
			/* minor */
			if(glfwGetTime() <= 30.0 || glfwGetTime() >= 32.0) {
				double modtime = dmod(glfwGetTime() * 4 + SPT / 2.0, SPT);
				double pop = cubic_bezier_out(modtime, 1.0, 0.0, 1.0, 0.0);
				flash += pop * 0.1;
			}
			/* major */
			double modtime = 99.0;
			if(glfwGetTime() <= 28.0) {
				modtime = dmod(glfwGetTime() * 2.0, SPT * 4.0);
			} else if(glfwGetTime() <= 50.0) {
				if(glfwGetTime() > 31.0 && glfwGetTime() <= 33.0) {
					modtime = dmod(glfwGetTime() * 2.0 + SPT, SPT * 2.0);
				} else if(glfwGetTime() >= 45.0) {
					modtime = dmod(glfwGetTime() * 2.0 + SPT, SPT * 2.0);
				}
			} else if(glfwGetTime() <= 63.0) {
				modtime = dmod(glfwGetTime() * 2.0 - SPT, SPT * 8.0);
			} else if(glfwGetTime() <= 71.0) {
				modtime = dmod(glfwGetTime() * 2.0 + SPT, SPT * 2.0);
			}
			double pop = cubic_bezier_out(modtime, 0.0, 0.0, 0.8, 0.1);
			flash += pop;
#if 1
			set_uniform1f(vignette_white, min(flash, 1.0));
#else
			set_uniform1f(vignette_white, 0.0);
#endif
		}
		render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP);
		unbind_texture2d();
		unbind_program();

		/* crt on a to b */
		unbind_fbo();
		bind_fbo(fbo_b);
		bind_program(crt_program);
		set_uniform1i(crt_texture, active_texture(0));
		bind_texture2d(fbo_tex_a);
		set_uniform2f(crt_scale, (vec2){ 1.0, 1.0 });
		set_uniform2f(crt_offset, (vec2){ 0.0, 0.0 });
		render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP);
		unbind_texture2d();
		unbind_program();

		gl(Viewport(0, 0, WIDTH, HEIGHT));
		/* bloom on b to screen */
		unbind_fbo();
		gl(Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
		bind_program(bloom_program);
		set_uniform1i(bloom_texture, active_texture(0));
		bind_texture2d(fbo_tex_b);
		/* pop effect */
		/* special cases: 30 - 32 */
		if(glfwGetTime() <= 30.0 || glfwGetTime() >= 31.5) {
			double modtime = dmod(glfwGetTime(), SPT / 4.0);
			double pop = ease_out_spring(modtime, 0.5, 0.0, 1.0);
#if 1
			set_uniform2f(bloom_scale, (vec2){ 1.0 + pop * 0.06, 1.0 + pop * 0.06 });
#else
			set_uniform2f(bloom_scale, (vec2){ 1.0, 1.0 });
#endif
		} else {
            set_uniform2f(bloom_scale, (vec2){ 0.97, 0.97 });
		}
		set_uniform2f(bloom_offset, (vec2){ 0.0, 0.0 });
		set_uniform1f(bloom_size, 1.0 / 128.0);
		set_uniform1f(bloom_intensity, 0.25);
		render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP);
		unbind_texture2d();
		unbind_program();

		/* display */
		glfwSwapBuffers(window);
	}

	/* signal to the music thread to stop */
#ifdef OS_LIN
	pthread_mutex_lock(&g_thread_lock);
	tg_playing = 0;
	pthread_mutex_unlock(&g_thread_lock);

	pthread_join(thread_id, NULL);

	pthread_mutex_destroy(&g_thread_lock);
#endif
#ifdef OS_WIN
	WaitForSingleObject(g_thread_lock, INFINITE);
	tg_playing = 0;
	ReleaseMutex(g_thread_lock);

	WaitForSingleObject(thread_handle, INFINITE);
	CloseHandle(thread_handle);
	CloseHandle(g_thread_lock);
#endif

    setdown_graphics();

	return 0;
}
