#include "graphics.h"
#include "dat/glsl/shaders.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int load_program(program_t* program, const char* vert_src, const char* frag_src) {
	gfx_assert(program->handle != GLHANDLE);

	shader_t vert = ISHADER, frag = ISHADER;
	create_shader(&vert, GL_VERTEX_SHADER);
	create_shader(&frag, GL_FRAGMENT_SHADER);

	/* compile */
	const char* vert_result = compile_shader(vert, vert_src);
	if(vert_result != NULL) {
		fprintf(stderr, "failed to load vertex shader:\n%s", vert_result);
		#if 0
		free((char*)vert_result);
		destroy_shader(&vert);
		destroy_shader(&frag);
		#else
		exit(0);
		#endif
		return 0;
	}

	const char* frag_result = compile_shader(frag, frag_src);
	if(frag_result != NULL) {
		fprintf(stderr, "failed to load fragment shader:\n%s", frag_result);
		#if 0
		free((char*)frag_result);
		destroy_shader(&vert);
		destroy_shader(&frag);
		#else
		exit(0);
		#endif
		return 0;
	}

	/* link */
	const char* link_result = link_program(*program, vert, frag);
	if(link_result != NULL) {
		fprintf(stderr, "failed to link program:\n%s", link_result);
		#if 0
		free((char*)link_result);
		destroy_shader(&vert);
		destroy_shader(&frag);
		#else
		exit(0);
		#endif
		return 0;
	}

	destroy_shader(&vert);
	destroy_shader(&frag);

	return 1;
}

#if 0
int load_program_files(program_t* program, const char* vert_filename, const char* frag_filename) {
	char* vert_contents = load_file(vert_filename);
	if(!vert_contents) {
		return 0;
	}

	char* frag_contents = load_file(frag_filename);
	if(!frag_contents) {
		free(vert_contents);
		return 0;
	}

	int ret = load_program(program, vert_contents, frag_contents);

	free(vert_contents);
	free(frag_contents);

	if(!ret) fprintf(stderr, "failed to load %s or %s\n", vert_filename, frag_filename);

	return ret;
}
#endif

GLFWwindow* window = NULL;

/* render a front-facing quad using render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP); */
mesh2d_t quad = IMESH2D;

texture2d_t c64_texture = ITEXTURE2D;

/* post processing */
texture2d_t fbo_tex_a = ITEXTURE2D;
rbo_t rbo_a = IRBO;
fbo_t fbo_a = IFBO;

texture2d_t fbo_tex_b = ITEXTURE2D;
rbo_t rbo_b = IRBO;
fbo_t fbo_b = IFBO;

program_t fancy_program = IPROGRAM;
attribute_t fancy_quad_cs = IATTRIBUTE;
uniform_t fancy_scale = IUNIFORM;
uniform_t fancy_offset = IUNIFORM;
uniform_t fancy_blit_texture = IUNIFORM;
uniform_t fancy_uv_offset = IUNIFORM;
uniform_t fancy_uv_scale = IUNIFORM;
uniform_t fancy_alpha = IUNIFORM;
uniform_t fancy_mvp = IUNIFORM;

program_t plasma_program = IPROGRAM;
attribute_t plasma_quad_cs = IATTRIBUTE;
uniform_t plasma_scale = IUNIFORM;
uniform_t plasma_offset = IUNIFORM;
uniform_t plasma_time = IUNIFORM;

program_t invplasma_program = IPROGRAM;
attribute_t invplasma_quad_cs = IATTRIBUTE;
uniform_t invplasma_scale = IUNIFORM;
uniform_t invplasma_offset = IUNIFORM;
uniform_t invplasma_time = IUNIFORM;

program_t fly_program = IPROGRAM;
attribute_t fly_quad_cs = IATTRIBUTE;
uniform_t fly_scale = IUNIFORM;
uniform_t fly_offset = IUNIFORM;
uniform_t fly_time = IUNIFORM;

program_t crt_program = IPROGRAM;
attribute_t crt_quad_cs = IATTRIBUTE;
uniform_t crt_scale = IUNIFORM;
uniform_t crt_offset = IUNIFORM;
uniform_t crt_texture = IUNIFORM;

program_t vignette_program = IPROGRAM;
attribute_t vignette_quad_cs = IATTRIBUTE;
uniform_t vignette_scale = IUNIFORM;
uniform_t vignette_offset = IUNIFORM;
uniform_t vignette_texture = IUNIFORM;
uniform_t vignette_white = IUNIFORM;

program_t bloom_program = IPROGRAM;
attribute_t bloom_quad_cs = IATTRIBUTE;
uniform_t bloom_scale = IUNIFORM;
uniform_t bloom_offset = IUNIFORM;
uniform_t bloom_texture = IUNIFORM;
uniform_t bloom_size = IUNIFORM;
uniform_t bloom_intensity = IUNIFORM;

program_t cradle_program = IPROGRAM;
attribute_t cradle_quad_cs = IATTRIBUTE;
uniform_t cradle_scale = IUNIFORM;
uniform_t cradle_offset = IUNIFORM;
uniform_t cradle_time = IUNIFORM;
uniform_t cradle_state = IUNIFORM;

int load_programs() {
	/* fancy text shader */
	load_program(&fancy_program, fancy_vert, fancy_frag);
	fancy_quad_cs = get_attribute(fancy_program, "quad_cs");
	fancy_scale = get_uniform(fancy_program, "scale");
	fancy_offset = get_uniform(fancy_program, "offset");
	fancy_blit_texture = get_uniform(fancy_program, "blit_texture");
	fancy_uv_offset = get_uniform(fancy_program, "uv_offset");
	fancy_uv_scale = get_uniform(fancy_program, "uv_scale");
	fancy_alpha = get_uniform(fancy_program, "alpha");
	fancy_mvp = get_uniform(fancy_program, "mvp");
	bind_program(fancy_program);
	attach_mesh2d(&quad, (attribute_t*){ &fancy_quad_cs });
	unbind_program();

	/* plasma shader */
	load_program(&plasma_program, blit_vert, plasma_frag);
	plasma_quad_cs = get_attribute(plasma_program, "quad_cs");
	plasma_scale = get_uniform(plasma_program, "scale");
	plasma_offset = get_uniform(plasma_program, "offset");
	plasma_time = get_uniform(plasma_program, "time");
	bind_program(plasma_program);
	attach_mesh2d(&quad, (attribute_t*){ &plasma_quad_cs });
	unbind_program();

	/* inverse plasma shader */
	load_program(&invplasma_program, blit_vert, invz_plasma_frag);
	invplasma_quad_cs = get_attribute(invplasma_program, "quad_cs");
	invplasma_scale = get_uniform(invplasma_program, "scale");
	invplasma_offset = get_uniform(invplasma_program, "offset");
	invplasma_time = get_uniform(invplasma_program, "time");
	bind_program(invplasma_program);
	attach_mesh2d(&quad, (attribute_t*){ &invplasma_quad_cs });
	unbind_program();

	/* fly shader */
	load_program(&fly_program, blit_vert, fly_frag);
	fly_quad_cs = get_attribute(fly_program, "quad_cs");
	fly_scale = get_uniform(fly_program, "scale");
	fly_offset = get_uniform(fly_program, "offset");
	fly_time = get_uniform(fly_program, "time");
	bind_program(fly_program);
	attach_mesh2d(&quad, (attribute_t*){ &fly_quad_cs });
	unbind_program();

	/* crt shader */
	load_program(&crt_program, blit_vert, crt_frag);
	crt_quad_cs = get_attribute(crt_program, "quad_cs");
	crt_scale = get_uniform(crt_program, "scale");
	crt_offset = get_uniform(crt_program, "offset");
	crt_texture = get_uniform(crt_program, "tex");
	bind_program(crt_program);
	attach_mesh2d(&quad, (attribute_t*){ &crt_quad_cs });
	unbind_program();

	/* vignette shader */
	load_program(&vignette_program, blit_vert, vignette_frag);
	vignette_quad_cs = get_attribute(vignette_program, "quad_cs");
	vignette_scale = get_uniform(vignette_program, "scale");
	vignette_offset = get_uniform(vignette_program, "offset");
	vignette_texture = get_uniform(vignette_program, "tex");
	vignette_white = get_uniform(vignette_program, "white");
	bind_program(vignette_program);
	attach_mesh2d(&quad, (attribute_t*){ &vignette_quad_cs });
	unbind_program();

	/* bloom shader */
	load_program(&bloom_program, blit_vert, bloom_frag);
	bloom_quad_cs = get_attribute(bloom_program, "quad_cs");
	bloom_scale = get_uniform(bloom_program, "scale");
	bloom_offset = get_uniform(bloom_program, "offset");
	bloom_texture = get_uniform(bloom_program, "tex");
	bloom_size = get_uniform(bloom_program, "blur_size");
	bloom_intensity = get_uniform(bloom_program, "intensity");
	bind_program(bloom_program);
	attach_mesh2d(&quad, (attribute_t*){ &bloom_quad_cs });
	unbind_program();

	/* cradle shader */
	load_program(&cradle_program, blit_vert, cradle_frag);
	cradle_quad_cs = get_attribute(cradle_program, "quad_cs");
	cradle_scale = get_uniform(cradle_program, "scale");
	cradle_offset = get_uniform(cradle_program, "offset");
	cradle_time = get_uniform(cradle_program, "time");
	cradle_state = get_uniform(cradle_program, "state");
	bind_program(cradle_program);
	attach_mesh2d(&quad, (attribute_t*){ &cradle_quad_cs });
	unbind_program();

	return 1;
}

void create_programs() {
	create_program(&fancy_program);
	create_program(&plasma_program);
	create_program(&invplasma_program);
	create_program(&fly_program);
	create_program(&vignette_program);
	create_program(&crt_program);
	create_program(&bloom_program);
	create_program(&cradle_program);
}

void destroy_programs() {
	destroy_program(&fancy_program);
	destroy_program(&plasma_program);
	destroy_program(&invplasma_program);
	destroy_program(&fly_program);
	destroy_program(&vignette_program);
	destroy_program(&crt_program);
	destroy_program(&bloom_program);
	destroy_program(&cradle_program);
}

int setup_graphics() {
	gfx_assert(window == NULL);

	/* init glfw */
	if(!glfwInit()) {
		fprintf(stderr, "failed to init glfw.\n");
		return 0;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	/* create the glfw window before gl calls */
	glfwWindowHint(GLFW_RESIZABLE, 0);
	window = glfwCreateWindow(WIDTH, HEIGHT, "demo", NULL, NULL);
	if(window == NULL) {
		fprintf(stderr, "failed to open glfw window. is opengl 3.3 supported?\n");
		glfwTerminate();
		return 0;
	}
	glfwPollEvents();
	glfwMakeContextCurrent(window);

	/* initialize the opengl context */
	if(gl3wInit() != 0) {
		fprintf(stderr, "failed to initialize gl3w. is opengl 3.3 supported?\n");
		glfwDestroyWindow(window);
		glfwTerminate();
		return 0;
	}

	/* make sure 3.3 is supported */
	if(!gl3wIsSupported(3, 3)) {
		fprintf(stderr, "opengl 3.3 is not supported.\n");
		glfwDestroyWindow(window);
		glfwTerminate();
		return 0;
	}

	/* quad */
	create_mesh2d(&quad);
	quad.vertices_size = 4;
	static vec2 quad_data[4] = {
		{ -1.0, -1.0 },
		{  1.0, -1.0 },
		{ -1.0,  1.0 },
		{  1.0,  1.0 }
	};
	quad.vertices = quad_data;
	upload_mesh2d(&quad);

	/* texture */
	create_texture2d(&c64_texture);
	bind_texture2d(c64_texture);
	set_filter_texture2d(GL_NEAREST);
 	set_wrap_texture2d(GL_CLAMP_TO_BORDER);
	/* by default opengl expects the # of bytes in each row to be a multiple of 4 */
	/* let opengl know that this texture doesn't have such padding */
	gl(PixelStorei(GL_UNPACK_ALIGNMENT, 1));
	/* the 8 bit font data describes the alpha channel, tell opengl that */
	GLint font_mask[] = { GL_ONE, GL_ONE, GL_ONE, GL_RED };
	gl(TexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, font_mask));
	upload_texture2d(&c64_font[0], C64_X * strlen(c64_font_content), C64_Y, GL_UNSIGNED_BYTE, GL_RED, GL_R8);
	gl(PixelStorei(GL_UNPACK_ALIGNMENT, 4));
	unbind_texture2d();

	/* post processing, a */
	create_texture2d(&fbo_tex_a);
	bind_texture2d(fbo_tex_a);
	set_filter_texture2d(GL_LINEAR);
	set_wrap_texture2d(GL_CLAMP_TO_BORDER);
	upload_texture2d(NULL, WIDTH/2, HEIGHT/2, GL_UNSIGNED_BYTE, GL_RGBA, GL_RGBA8);
	unbind_texture2d();

	create_rbo(&rbo_a);
	bind_rbo(rbo_a);
	store_rbo(GL_DEPTH_COMPONENT24, WIDTH/2, HEIGHT/2);
	unbind_rbo();

	create_fbo(&fbo_a);
	bind_fbo(fbo_a);
	attach_texture2d_fbo(GL_COLOR_ATTACHMENT0, fbo_tex_a);
	attach_rbo_fbo(GL_DEPTH_ATTACHMENT, rbo_a);
	gfx_assert(fbo_status(fbo_a) == GL_FRAMEBUFFER_COMPLETE);
	unbind_fbo();

	/* b */
	create_texture2d(&fbo_tex_b);
	bind_texture2d(fbo_tex_b);
	set_filter_texture2d(GL_LINEAR);
	set_wrap_texture2d(GL_CLAMP_TO_BORDER);
	upload_texture2d(NULL, WIDTH/2, HEIGHT/2, GL_UNSIGNED_BYTE, GL_RGBA, GL_RGBA8);
	unbind_texture2d();

	create_rbo(&rbo_b);
	bind_rbo(rbo_b);
	store_rbo(GL_DEPTH_COMPONENT24, WIDTH/2, HEIGHT/2);
	unbind_rbo();

	create_fbo(&fbo_b);
	bind_fbo(fbo_b);
	attach_texture2d_fbo(GL_COLOR_ATTACHMENT0, fbo_tex_b);
	attach_rbo_fbo(GL_DEPTH_ATTACHMENT, rbo_b);
	gfx_assert(fbo_status(fbo_b) == GL_FRAMEBUFFER_COMPLETE);
	unbind_fbo();

	create_programs();

	return load_programs();
}

void setdown_graphics() {
	#if 0
	if(window == NULL) return;
	#endif

	/* destroy opengl objects */
	destroy_mesh2d(&quad);
	destroy_texture2d(&c64_texture);

	destroy_texture2d(&fbo_tex_a);
	destroy_rbo(&rbo_a);
	destroy_fbo(&fbo_a);

	destroy_texture2d(&fbo_tex_b);
	destroy_rbo(&rbo_b);
	destroy_fbo(&fbo_b);

	destroy_programs();

	/* destroy window after gl calls */
	glfwDestroyWindow(window); window = NULL;

	glfwTerminate();
}
