#include "utils.h"

#include <stdio.h>
#include <stdlib.h>

double rad(double a);
double ndc_y(double wc);
double ndc_x(double wc);
double dmod(double t, double m);
double lerp(double a, double t, double b);
point_t lerp_point(point_t* a, double t, point_t* b);

/* out */
double cubic_bezier_out(double t, double x0, double y0, double x1, double y1) {
	/* todo, tidy this up */
	if(t <= 0.0) return 1.0;
	if(t >= 1.0) return 0.0;
	static point_t a = { 1.0, 1.0 }, d = { 0.0, 0.0 };
	point_t b = { x0, y0 }, c = { x1, y1 };
	point_t ab = lerp_point(&a, t, &b);
	point_t bc = lerp_point(&b, t, &c);
	point_t cd = lerp_point(&c, t, &d);
	point_t abbc = lerp_point(&ab, t, &bc);
	point_t bccd = lerp_point(&bc, t, &cd);
	return lerp(abbc.y, t, bccd.y);
}

/* in */
#if 0
double cubic_bezier(double t, double x0, double y0, double x1, double y1) {
	/* todo, tidy this up */
	if(t <= 0.0) return 0.0;
	if(t >= 1.0) return 1.0;
	static point_t a = { 0.0, 0.0 }, d = { 1.0, 1.0 };
	point_t b = { x0, y0 }, c = { x1, y1 };
	point_t ab = lerp_point(&a, t, &b);
	point_t bc = lerp_point(&b, t, &c);
	point_t cd = lerp_point(&c, t, &d);
	point_t abbc = lerp_point(&ab, t, &bc);
	point_t bccd = lerp_point(&bc, t, &cd);
	return lerp(abbc.y, t, bccd.y);
}
#endif

/* taken from skaven's shader on shadertoy */
double ease_out_spring(double t, double b, double c, double d) {
	double s = 1.70158;
	double p = 0.0;
	double a = b;

	if(t == 0.0) return -c;
	if((t /= d) == 1.0) return -(c + b);
	if(p == 0.0) p = d * 0.3;
	if(a < abs(b)) {
		a = b;
		s = p / 4.0;
	} else {
		s = p / (2.0 * PI) * asin(b / a);
	}
	return -(a * pow(2.0, -10.0 * t) * sin((t * d - s) * (2.0 * PI) / p) + b + c);
}

#if 0
char* load_file(const char* file) {
	FILE* fp = fopen(file, "rb");
	if(fp == NULL) {
		fprintf(stderr, "failed to load \"%s\"", file);
		return NULL;
	}

	fseek(fp, 0, SEEK_END);
	size_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	char* contents = malloc(sizeof(*contents) * size + 1);
	if(contents == NULL) return NULL;

	fread(contents, size, 1, fp);
	contents[size] = (char)0;

	return contents;
}
#endif
