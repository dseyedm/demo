#include "sound.h"
#include "utils.h"
#include "dat/mod/crema.h"

#include <GLFW/glfw3.h>

extern double SPT;

modcontext modctx;
int tg_playing = 1;
short wave_buffers[NUM_BUFFERS][BUFFER_SAMPLES * NUM_CHANNELS];

#ifdef OS_LIN
size_t buffer_size = BUFFER_SAMPLES * NUM_CHANNELS * sizeof(short);
size_t period_size = BUFFER_SAMPLES * NUM_CHANNELS * sizeof(short);//buffer_size;
pthread_mutex_t g_thread_lock;
snd_pcm_t* device;
snd_pcm_format_t format;
snd_pcm_uframes_t nframes = BUFFER_SAMPLES * sizeof(short) / 2;//period_size / NUM_CHANNELS;

#define FATAL_ALSA(s, err) do { \
	fprintf(stderr, s ": %s\n", snd_strerror((err))); \
	exit(1); \
} while(0)

#define CHECK_ALSA(call) do { \
	int ret = (call); \
	if(ret < 0) { \
		ret = snd_pcm_recover(device, ret, 0); \
		if(ret < 0) { \
			FATAL_ALSA("ALSA internal error", ret); \
		} \
	} \
} while(0)
#endif

#ifdef OS_WIN
HANDLE semaphore;
void __stdcall wave_out_proc(HWAVEOUT hWaveOut, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2) {
	if(uMsg == WOM_DONE) ReleaseSemaphore(semaphore, 1, NULL);
}

HANDLE g_thread_lock;

WAVEFORMATEX wave_format;
WAVEHDR wave_headers[NUM_BUFFERS];
WAVEHDR* header;
HWAVEOUT h_wave_out;

#endif

void load_music() {
	hxcmod_init(&modctx);
	hxcmod_load(&modctx, (void*)crema, crema_size);

#ifdef OS_LIN
	device = NULL;
	format = SND_PCM_FORMAT_S16;
	void* params = NULL;
	CHECK_ALSA(snd_pcm_open(&device, "default", SND_PCM_STREAM_PLAYBACK, 0));
	CHECK_ALSA(snd_pcm_hw_params_malloc((snd_pcm_hw_params_t**)(&params)));
	CHECK_ALSA(snd_pcm_hw_params_any(device, params));
	CHECK_ALSA(snd_pcm_hw_params_set_access(device, params, SND_PCM_ACCESS_RW_INTERLEAVED));
	CHECK_ALSA(snd_pcm_hw_params_set_format(device, params, format));
	CHECK_ALSA(snd_pcm_hw_params_set_rate(device, params, SAMPLING_FREQ, 0));
	CHECK_ALSA(snd_pcm_hw_params_set_channels(device, params, NUM_CHANNELS));
	CHECK_ALSA(snd_pcm_hw_params_set_buffer_size_near(device, params, &buffer_size));
	CHECK_ALSA(snd_pcm_hw_params_set_period_size_near(device, params, &period_size, 0));
	CHECK_ALSA(snd_pcm_hw_params(device, params));
	snd_pcm_hw_params_free(params);

	CHECK_ALSA(snd_pcm_sw_params_malloc((snd_pcm_sw_params_t**)(&params)));
	CHECK_ALSA(snd_pcm_sw_params_current(device, params));
	CHECK_ALSA(snd_pcm_sw_params_set_start_threshold(device, params, buffer_size));
	CHECK_ALSA(snd_pcm_sw_params(device, params));
	snd_pcm_sw_params_free(params);
#endif

#ifdef OS_WIN
	wave_format.wFormatTag = WAVE_FORMAT_PCM;
	wave_format.nChannels = NUM_CHANNELS;
	wave_format.nSamplesPerSec = SAMPLING_FREQ;
	wave_format.nAvgBytesPerSec = SAMPLING_FREQ * NUM_CHANNELS * 2;
	wave_format.nBlockAlign = NUM_CHANNELS * 2;
	wave_format.wBitsPerSample = 16;

	for(long idx = 0; idx < NUM_BUFFERS; ++idx) {
		header = &wave_headers[idx];
		memset(header, 0, sizeof(WAVEHDR));
		header->lpData = (LPSTR)&wave_buffers[idx][0];
		header->dwBufferLength = BUFFER_SAMPLES * NUM_CHANNELS * sizeof(short);
	}

	semaphore = CreateSemaphore(NULL, NUM_BUFFERS, NUM_BUFFERS, NULL);

	waveOutOpen(&h_wave_out, WAVE_MAPPER, &wave_format, (DWORD_PTR)wave_out_proc, 0, CALLBACK_FUNCTION);
#endif
}

#ifdef OS_LIN
void* play_module_thread(void* thread_data) {
	UNUSED(thread_data);

	CHECK_ALSA(snd_pcm_prepare(device));
	long playing = 1, current_buffer = 0;
	glfwSetTime(0.0); /* let's race! */
	while(playing) {
		/* wait for the data mutex to become available */
		pthread_mutex_lock(&g_thread_lock);
		playing = tg_playing;
		pthread_mutex_unlock(&g_thread_lock);

		/* similar intro pattern, but different */
		if(modctx.tablepos == 21 && !modctx.patternpos) {
			glfwSetTime(0.0); /* let's race! */
		}

		short* buffer = &wave_buffers[current_buffer][0];
		memset(buffer, 0, BUFFER_SAMPLES * NUM_CHANNELS * sizeof(short));
		hxcmod_fillbuffer(&modctx, (unsigned short*)buffer, BUFFER_SAMPLES, NULL);

		/* repeat 2 patterns ahead */
		if(!modctx.tablepos && !modctx.patternpos) {
			modctx.tablepos = 2;
		}

		CHECK_ALSA(snd_pcm_writei(device, buffer, nframes));

		current_buffer = (current_buffer + 1) % NUM_BUFFERS;
	}

	hxcmod_unload(&modctx);

	CHECK_ALSA(snd_pcm_drop(device));
	CHECK_ALSA(snd_pcm_close(device));

	return NULL;
}
#endif

#ifdef OS_WIN
DWORD WINAPI play_module_thread(LPVOID param) {
	UNUSED(param);

	long playing = 1;
	long current_buffer = 0;
	glfwSetTime(0.0); /* let's race! */
	while(playing) {
		/* wait for the data flag to become available */
		WaitForSingleObject(g_thread_lock, INFINITE);
		playing = tg_playing;
		ReleaseMutex(g_thread_lock);

		/* wait for a buffer to become available */
		WaitForSingleObject(semaphore, INFINITE);

		/* similar intro pattern, but different */
		if(modctx.tablepos == 21 && !modctx.patternpos) {
			glfwSetTime(0.0); /* let's race! */
		}

		header = &wave_headers[current_buffer];
		short* buffer = &wave_buffers[current_buffer][0];

		/* clear the mix buffer and get audio */
		memset(buffer, 0, BUFFER_SAMPLES * NUM_CHANNELS * sizeof(short));
		hxcmod_fillbuffer(&modctx, (unsigned short*)buffer, BUFFER_SAMPLES, NULL);

		/* repeat 2 patterns ahead */
		if(!modctx.tablepos && !modctx.patternpos) {
			modctx.tablepos = 2;
		}

		/* submit buffer to audio system */
		waveOutUnprepareHeader(h_wave_out, header, sizeof(WAVEHDR));
		waveOutPrepareHeader(h_wave_out, header, sizeof(WAVEHDR));
		waveOutWrite(h_wave_out, header, sizeof(WAVEHDR));

		current_buffer = (current_buffer + 1) % NUM_BUFFERS;
	}

	/* drop any currently playing buffers */
	waveOutReset(h_wave_out);
	while(waveOutClose(h_wave_out) == WAVERR_STILLPLAYING) {
		/* keep attempting to close all buffers */
		Sleep(100);
	}

	hxcmod_unload(&modctx);

	return 0;
}

#endif
