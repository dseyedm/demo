#ifndef GRAPHICS_H
#define GRAPHICS_H

/* make sure to include gfx/context.h before glfw */
#include "gfx/context.h"
#include "gfx/assert.h"
#include "gfx/shader.h"
#include "gfx/texture.h"
#include "gfx/vertex_array.h"
#include "gfx/vertex_buffer.h"
#include "gfx/mesh.h"
#include "gfx/frame_buffer.h"

#include <GLFW/glfw3.h>

#include "utils.h"

#include "dat/tex/c64.h"

extern GLFWwindow* window;

extern mesh2d_t quad;

extern texture2d_t c64_texture;

extern texture2d_t fbo_tex_a;
extern rbo_t rbo_a;
extern fbo_t fbo_a;

extern texture2d_t fbo_tex_b;
extern rbo_t rbo_b;
extern fbo_t fbo_b;

extern program_t fancy_program;
extern attribute_t fancy_quad_cs;
extern uniform_t fancy_scale;
extern uniform_t fancy_offset;
extern uniform_t fancy_blit_texture;
extern uniform_t fancy_uv_offset;
extern uniform_t fancy_uv_scale;
extern uniform_t fancy_alpha;
extern uniform_t fancy_mvp;

extern program_t plasma_program;
extern attribute_t plasma_quad_cs;
extern uniform_t plasma_scale;
extern uniform_t plasma_offset;
extern uniform_t plasma_time;

extern program_t invplasma_program;
extern attribute_t invplasma_quad_cs;
extern uniform_t invplasma_scale;
extern uniform_t invplasma_offset;
extern uniform_t invplasma_time;

extern program_t fly_program;
extern attribute_t fly_quad_cs;
extern uniform_t fly_scale;
extern uniform_t fly_offset;
extern uniform_t fly_time;

extern program_t crt_program;
extern attribute_t crt_quad_cs;
extern uniform_t crt_scale;
extern uniform_t crt_offset;
extern uniform_t crt_texture;

extern program_t vignette_program;
extern attribute_t vignette_quad_cs;
extern uniform_t vignette_scale;
extern uniform_t vignette_offset;
extern uniform_t vignette_texture;
extern uniform_t vignette_white;

extern program_t bloom_program;
extern attribute_t bloom_quad_cs;
extern uniform_t bloom_scale;
extern uniform_t bloom_offset;
extern uniform_t bloom_texture;
extern uniform_t bloom_size;
extern uniform_t bloom_intensity;

extern program_t cradle_program;
extern attribute_t cradle_quad_cs;
extern uniform_t cradle_scale;
extern uniform_t cradle_offset;
extern uniform_t cradle_time;
extern uniform_t cradle_state;

/* always pair setup_graphics with setdown_graphics */
int setup_graphics();
/* can be called at any time in between setup and setdown */
int load_programs();
void create_programs();
void destroy_programs();
void setdown_graphics();

#endif
