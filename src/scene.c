#include "scene.h"

#include <string.h>

extern double SPT;

double time = 0.0;

void render_c64_fancy(const char* str, double wc_pos_x, double wc_pos_y, double scale, double alpha, mat4x4 mat) {
	wc_pos_x += 0.5 * scale * C64_X;
	wc_pos_y -= 0.5 * scale * C64_Y;

	bind_program(fancy_program);
	set_uniform1i(fancy_blit_texture, active_texture(0));
	bind_texture2d(c64_texture);
	set_uniform2f(fancy_scale, (vec2){ scale * C64_X / WIDTH, scale * C64_Y / HEIGHT });
	double one_over_c64_strlen = 1.0 / strlen(c64_font_content);
	set_uniform2f(fancy_uv_scale, (vec2){ one_over_c64_strlen, 1.0 });
	set_uniform1f(fancy_alpha, alpha);
	set_uniform4x4f(fancy_mvp, mat);

	double ndc_pos_y = ndc_y(wc_pos_y);

	int strlen_str = strlen(str);
	for(int i = 0; i < strlen_str; ++i) {
		int j = 0;
		while(j < strlen(c64_font_content) && c64_font_content[j] != str[i]) { ++j; }

		double ndc_pos_x = ndc_x(wc_pos_x + i * C64_X * scale);

		set_uniform2f(fancy_offset, (vec2){
			(cos((double)i / 5.0 * 0.5*PI + 2*PI * time / SPT * 4) * C64_X / 2.0 * scale) / WIDTH + ndc_pos_x,
			(sin((double)i / 5.0 * 0.5*PI + 2*PI * time / SPT * 4) * C64_Y / 2.0 * scale) / HEIGHT + ndc_pos_y
		});
		set_uniform2f(fancy_uv_offset, (vec2){ j * one_over_c64_strlen, 0.0 });
		render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP);
	}

	unbind_texture2d();
	unbind_program();
}

void cradle(int state) {
	bind_program(cradle_program);
	set_uniform2f(cradle_scale, (vec2){ 1.0, 1.0 });
	set_uniform2f(cradle_offset, (vec2){ 0.0, 0.0 });
	set_uniform1f(cradle_time, time / SPT * 4.0);
	set_uniform1i(cradle_state, state);
	render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP);
	unbind_program();
}

void render_plasma() {
	bind_program(plasma_program);
	set_uniform2f(plasma_scale, (vec2){ 1.0, 1.0 });
	set_uniform2f(plasma_offset, (vec2){ 0.0, 0.0 });
	set_uniform1f(plasma_time, time / SPT * 2.0);
	render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP);
	unbind_program();
}

void render_invz_plasma() {
	bind_program(invplasma_program);
	set_uniform2f(invplasma_scale, (vec2){ 1.0, 1.0 });
	set_uniform2f(invplasma_offset, (vec2){ 0.0, 0.0 });
	set_uniform1f(invplasma_time, time / SPT * 4.0);
	render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP);
	unbind_program();
}

void fly() {
	bind_program(fly_program);
	set_uniform2f(fly_scale, (vec2){ 1.0, 1.0 });
	set_uniform2f(fly_offset, (vec2){ 0.0, 0.0 });
	set_uniform1f(fly_time, time / SPT * 4.0);
	render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP);
	unbind_program();
}

double scl = 6.0;
void render_text(const char* t, double yoff) {
	for(int i = 0; i < 5; ++i) {
		mat4x4 model; mat4x4_identity(model);
		mat4x4_translate(model, 0.0, 0.0, -i * 0.01);

		mat4x4 cam_view;
		mat4x4_look_at(
			cam_view,
			(vec3){ sin(time / SPT * PI + PI/2.0), cos(time / SPT * PI+PI/2.0)*0.2 + 0.5, 0.8 },
			(vec3){ 0.0, 0.0, 0.0 },
			(vec3){ 0.0, 1.0, 0.0 }
		);

		mat4x4 cam_proj;
		mat4x4_perspective(cam_proj, rad(90.0), (double)WIDTH / HEIGHT, 0.1, 1000.0);

		mat4x4 mvp; /* = cam_proj * cam_view * model */
		mat4x4_mul(mvp, cam_proj, cam_view);
		mat4x4_mul(mvp, mvp, model);

		render_c64_fancy(
			t,
			WIDTH/2-strlen(t)/2.0*C64_X*scl,
			HEIGHT-HEIGHT/2+C64_Y*scl-yoff*scl,
			scl,
			1.0,
			mvp
		);
	}
}

int without = 0;
void scene0() {
	time = glfwGetTime();
	render_invz_plasma();
	if(!without) {
		scl = 12.0;
		render_text("Hello world!", 0);
	} else {
		scl = 11.0;
		render_text("without me...", 0);

	}
}

void scene1() {
	time = glfwGetTime();
	fly();
	scl = 11.0;
	render_text("Soraj proudly", 0);
	render_text("presents", 1.3*C64_Y);
}

void scene2() {
	time = glfwGetTime();
	cradle(1);
	scl = 9.0;
	render_text("a gift for", 0);
	scl = 12.0;
	render_text("Nathan", 1.3*C64_Y);
}

void scene3() {
	time = glfwGetTime() - 1.1*SPT;
	cradle(0);
	scl = 9.0;
	time = glfwGetTime();
	render_text("Released", 0);
	render_text("Christmas 2015", 1.3*C64_Y);
}

void scene4() {
	time = glfwGetTime();
	cradle(3);
}

void scene5() {
	time = glfwGetTime();
	cradle(4);
}

void scene6() {
	time = glfwGetTime();
	render_plasma();
	scl = 9.0;
	time += 3.5*SPT;
	render_text("Thanks for the", 0);
	scl = 9.0;
	render_text("memories", 1.3*C64_Y);
}

void scene7() {
	time = glfwGetTime();
	render_invz_plasma();
	scl = 12.0;
	time += 3.5*SPT;
	render_text("and laughs!", 0);
}

void scene8() {
	time = glfwGetTime();
	fly();
	scl = 12.0;
	time += 3.5*SPT;
	render_text("Enjoy 2016!", 0);
	without = 1;
}
