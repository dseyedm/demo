#include "gfx/vertex_buffer.h"

const vbo_t VBO = IVBO;

extern inline void create_vbo(vbo_t* vbo);
extern inline void destroy_vbo(vbo_t* vbo);

extern inline void bind_element_vbo(vbo_t vbo);
extern inline void unbind_element_vbo();
extern inline vbo_t bound_element_vbo();

extern inline void bind_array_vbo(vbo_t vbo);
extern inline void unbind_array_vbo();
extern inline vbo_t bound_array_vbo();

extern inline void upload_element_vbo8(uint8_t* data, size_t size);
extern inline void upload_element_vbo16(uint16_t* data, size_t size);
extern inline void upload_element_vbo32(uint32_t* data, size_t size);

extern inline void upload_array_vbo1fa(float* data, size_t size);
extern inline void upload_array_vbo2fa(vec2* data, size_t size);
extern inline void upload_array_vbo3fa(vec3* data, size_t size);
extern inline void upload_array_vbo4fa(vec4* data, size_t size);

extern inline void render_element_vbo8(size_t size, GLenum mode);
extern inline void render_element_vbo16(size_t size, GLenum mode);
extern inline void render_element_vbo32(size_t size, GLenum mode);
extern inline void render_array_vbo(size_t size, GLenum mode);
