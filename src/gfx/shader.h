#ifndef GFX_SHADER_H
#define GFX_SHADER_H

#include "gfx/context.h"
#include "gfx/vertex_array.h"

#include <linmath/linmath.h>

typedef struct {
	GLuint handle;
} shader_t;

#define ISHADER { GLHANDLE }
extern const shader_t SHADER;

/* caller must free returned string */
const char* compile_shader(shader_t shader, const char* source);
/* caller must free returned string */
const char* get_shader_src(shader_t shader);

typedef struct {
	GLuint handle;
} program_t;

#define IPROGRAM { GLHANDLE }
extern const program_t PROGRAM;

/* caller must free returned string */
const char* link_program(program_t program, shader_t vertex, shader_t fragment);

typedef struct {
	GLuint handle;
} uniform_t;

#define IUNIFORM { GLHANDLE }
extern const uniform_t UNIFORM;

typedef struct {
	GLuint handle;
} attribute_t;

#define IATTRIBUTE { GLHANDLE }
extern const attribute_t ATTRIBUTE;

typedef struct {
	GLuint handle;
	char* name;
} uniform_name_t;

typedef struct {
	GLuint handle;
	char* name;
} attribute_name_t;

/* caller must free each .name and the array itself */
int get_uniforms(program_t program, uniform_name_t** uniforms_out, size_t* size_out);
/* caller must free each .name and the array itself */
int get_attributes(program_t program, attribute_name_t** attributes_out, size_t* size_out);

inline int get_uniform_count(program_t program) {
	gfx_assert(program.handle != GLHANDLE);
	GLint count;
	gl(GetProgramiv(program.handle, GL_ACTIVE_UNIFORMS, &count));
	return count;
}

inline int get_attribute_count(program_t program) {
	gfx_assert(program.handle != GLHANDLE);
	GLint count;
	gl(GetProgramiv(program.handle, GL_ACTIVE_ATTRIBUTES, &count));
	return count;
}

inline void create_shader(shader_t* shader, GLenum type) {
	gfx_assert(shader->handle == GLHANDLE);
	shader->handle = gl(CreateShader(type));
}

inline void destroy_shader(shader_t* shader) {
	gfx_assert(shader->handle != GLHANDLE);
	gl(DeleteShader(shader->handle));
	*shader = SHADER;
}

inline void create_program(program_t* program) {
	gfx_assert(program->handle == GLHANDLE);
	program->handle = gl(CreateProgram());
}

inline void destroy_program(program_t* program) {
	gfx_assert(program->handle != GLHANDLE);
	gl(DeleteProgram(program->handle));
	*program = PROGRAM;
}

inline void set_attribute_handle(program_t program, const char* id, GLuint handle) {
	gfx_assert(program.handle != GLHANDLE);
	gl(BindAttribLocation(program.handle, handle, id));
}

inline void bind_program(program_t program) {
	gfx_assert(program.handle != GLHANDLE);
	gl(UseProgram(program.handle));
}

inline void unbind_program() {
	gl(UseProgram(0));
}

inline uniform_t get_uniform(program_t program, const char* id) {
	gfx_assert(program.handle != GLHANDLE);
	uniform_t uniform;
	uniform.handle = gl(GetUniformLocation(program.handle, id));
	return uniform;
}

inline attribute_t get_attribute(program_t program, const char* id) {
	gfx_assert(program.handle != GLHANDLE);
	attribute_t attribute;
	attribute.handle = gl(GetAttribLocation(program.handle, id));
	return attribute;
}

inline void enable_attribute(attribute_t attribute) {
	gfx_assert(attribute.handle != GLHANDLE);
	gfx_assert(bound_vao().handle != GLHANDLE);
	gl(EnableVertexAttribArray(attribute.handle));
}

inline void disable_attribute(attribute_t attribute) {
	gfx_assert(attribute.handle != GLHANDLE);
	gfx_assert(bound_vao().handle != GLHANDLE);
	gl(DisableVertexAttribArray(attribute.handle));
}

inline void set_attribute_ptr(attribute_t attribute, GLenum type, GLuint components) {
	gfx_assert(attribute.handle != GLHANDLE);
	gl(VertexAttribPointer(attribute.handle, components, type, GL_FALSE, 0, (GLvoid*)NULL));
}

inline void set_attribute1f(attribute_t attribute, float v) {
	gfx_assert(attribute.handle != GLHANDLE);
	gl(VertexAttrib1f(attribute.handle, v));
}

inline void set_attribute2f(attribute_t attribute, vec2 v) {
	gfx_assert(attribute.handle != GLHANDLE);
	gl(VertexAttrib2f(attribute.handle, v[0], v[1]));
}

inline void set_attribute3f(attribute_t attribute, vec3 v) {
	gfx_assert(attribute.handle != GLHANDLE);
	gl(VertexAttrib3f(attribute.handle, v[0], v[1], v[2]));
}

inline void set_attribute4f(attribute_t attribute, vec4 v) {
	gfx_assert(attribute.handle != GLHANDLE);
	gl(VertexAttrib4f(attribute.handle, v[0], v[1], v[2], v[3]));
}

inline void set_uniform1i(uniform_t uniform, int v) {
	gfx_assert(uniform.handle != GLHANDLE);
	gl(Uniform1i(uniform.handle, v));
}

inline void set_uniform1f(uniform_t uniform, float v) {
	gfx_assert(uniform.handle != GLHANDLE);
	gl(Uniform1f(uniform.handle, v));
}

inline void set_uniform2f(uniform_t uniform, vec2 v) {
	gfx_assert(uniform.handle != GLHANDLE);
	gl(Uniform2f(uniform.handle, v[0], v[1]));
}

inline void set_uniform3f(uniform_t uniform, vec3 v) {
	gfx_assert(uniform.handle != GLHANDLE);
	gl(Uniform3f(uniform.handle, v[0], v[1], v[2]));
}

inline void set_uniform4f(uniform_t uniform, vec4 v) {
	gfx_assert(uniform.handle != GLHANDLE);
	gl(Uniform4f(uniform.handle, v[0], v[1], v[2], v[3]));
}

inline void set_uniform4x4f(uniform_t uniform, mat4x4 v) {
	gfx_assert(uniform.handle != GLHANDLE);
	gl(UniformMatrix4fv(uniform.handle, 1, GL_FALSE, &v[0][0]));
}

#endif
