#ifndef GFX_ASSERT_H
#define GFX_ASSERT_H

#ifdef NDEBUG
#define gfx_assert(c) ((void)0)
#else
#define gfx_assert(c) ((c) ? (void)0 : _gfx_assert(#c, __FILE__, __FUNCTION__, __LINE__))
void _gfx_assert(const char* condition, const char* file, const char* function, int line);
#endif

#endif
