#ifndef GFX_RENDER_BUFFER_H
#define GFX_RENDER_BUFFER_H

#include "gfx/context.h"

typedef struct {
	GLuint handle;
} rbo_t;

#define IRBO { GLHANDLE }
extern const rbo_t RBO;

inline void create_rbo(rbo_t* rbo) {
	gfx_assert(rbo->handle == GLHANDLE);
	gl(GenRenderbuffers(1, &rbo->handle));
}

inline void destroy_rbo(rbo_t* rbo) {
	gfx_assert(rbo->handle != GLHANDLE);
	gl(DeleteRenderbuffers(1, &rbo->handle));
	*rbo = RBO;
}

inline void bind_rbo(rbo_t rbo) {
	gfx_assert(rbo.handle != GLHANDLE);
	gl(BindRenderbuffer(GL_RENDERBUFFER, rbo.handle));
}

inline void unbind_rbo() {
	gl(BindRenderbuffer(GL_RENDERBUFFER, 0));
}

inline rbo_t bound_rbo() {
	GLint bound;
	gl(GetIntegerv(GL_RENDERBUFFER_BINDING, &bound));
	rbo_t rbo = RBO;
	if(bound > 0) rbo.handle = bound;
	return rbo;
}

inline void store_rbo(GLenum internal_format, GLsizei width, GLsizei height) {
	gfx_assert(bound_rbo().handle != GLHANDLE);
	gl(RenderbufferStorage(GL_RENDERBUFFER, internal_format, width, height));
}

#endif
