#include "gfx/shader.h"

const shader_t SHADER = ISHADER;
const program_t PROGRAM = IPROGRAM;
const uniform_t UNIFORM = IUNIFORM;
const attribute_t ATTRIBUTE = IATTRIBUTE;

extern inline void create_shader(shader_t* shader, GLenum type);
extern inline void destroy_shader(shader_t* shader);

extern inline void create_program(program_t* program);
extern inline void destroy_program(program_t* program);

extern inline void set_attribute_handle(program_t program, const char* id, GLuint handle);

extern inline void bind_program(program_t program);
extern inline void unbind_program();

extern inline int get_uniform_count(program_t program);
extern inline int get_attribute_count(program_t program);

extern inline uniform_t get_uniform(program_t program, const char* id);
extern inline attribute_t get_attribute(program_t program, const char* id);

extern inline void enable_attribute(attribute_t attribute);
extern inline void disable_attribute(attribute_t attribute);

extern inline void set_attribute_ptr(attribute_t attribute, GLenum type, GLuint components);
extern inline void set_attribute1f(attribute_t attribute, float v);
extern inline void set_attribute2f(attribute_t attribute, vec2 v);
extern inline void set_attribute3f(attribute_t attribute, vec3 v);
extern inline void set_attribute4f(attribute_t attribute, vec4 v);

/* ommitted set_uniform#[i/u](a), they are rarely used */

extern inline void set_uniform1i(uniform_t uniform, int i);

extern inline void set_uniform1f(uniform_t uniform, float v);
extern inline void set_uniform2f(uniform_t uniform, vec2 v);
extern inline void set_uniform3f(uniform_t uniform, vec3 v);
extern inline void set_uniform4f(uniform_t uniform, vec4 v);

/* ommitted set_uniform#fa, they are rarely used */

/* ommitted mat#x#, mat4x4 is mostly used */
extern inline void set_uniform4x4f(uniform_t uniform, mat4x4 v);

/* ommitted set_uniform#x#fa, they are rarely used */

#include <stdlib.h>
#include <string.h>

const char* compile_shader(shader_t shader, const char* source) {
	gfx_assert(shader.handle != GLHANDLE);

	gl(ShaderSource(shader.handle, 1, &source, NULL));
	gl(CompileShader(shader.handle));
	GLint compiled = GL_FALSE;
	gl(GetShaderiv(shader.handle, GL_COMPILE_STATUS, &compiled));
	if(compiled != GL_TRUE) {
		GLint info_len;
		gl(GetShaderiv(shader.handle, GL_INFO_LOG_LENGTH, &info_len));
		char* info = malloc(sizeof(*info) * (info_len + 1));
		if(info == NULL) return info;
		/* the string that will be returned is NULL terminated */
		gl(GetShaderInfoLog(shader.handle, info_len, NULL, info));
		return info;
	}
	return NULL;
}

const char* get_shader_src(shader_t shader) {
	gfx_assert(shader.handle != GLHANDLE);

	GLsizei src_len;
	gl(GetShaderiv(shader.handle, GL_SHADER_SOURCE_LENGTH, &src_len));
	char* src = malloc(sizeof(*src) * (src_len + 1));
	if(src == NULL) return src;
	/* the string that will be returned is NULL terminated */
	gl(GetShaderSource(shader.handle, src_len, NULL, src));
	return src;
}

const char* link_program(program_t program, shader_t vertex, shader_t fragment) {
	gfx_assert(program.handle != GLHANDLE);
	gfx_assert(vertex.handle != GLHANDLE);
	gfx_assert(fragment.handle != GLHANDLE);

	gl(AttachShader(program.handle, vertex.handle));
	gl(AttachShader(program.handle, fragment.handle));
	gl(LinkProgram(program.handle));
	GLint linked = GL_FALSE;
	gl(GetProgramiv(program.handle, GL_LINK_STATUS, &linked));
	if(linked != GL_TRUE) {
		GLint info_len;
		gl(GetProgramiv(program.handle, GL_INFO_LOG_LENGTH, &info_len));
		char* info = malloc(sizeof(*info) * (info_len + 1));
		/* the string that will be returned is NULL terminated */
		gl(GetProgramInfoLog(program.handle, info_len, NULL, info));
		gl(DetachShader(program.handle, vertex.handle));
		gl(DetachShader(program.handle, fragment.handle));
		return info;
	}
	gl(DetachShader(program.handle, vertex.handle));
	gl(DetachShader(program.handle, fragment.handle));
	return NULL;
}

int get_uniforms(program_t program, uniform_name_t** uniforms_out, size_t* size_out) {
	gfx_assert(program.handle != GLHANDLE);

	GLint count, max_len;
	count = get_uniform_count(program);
	gl(GetProgramiv(program.handle, GL_ACTIVE_UNIFORM_MAX_LENGTH, &max_len));
	uniform_name_t* uniforms = malloc(sizeof(*uniforms) * count);
	if(uniforms == NULL) return 0;
	char temp_name[max_len + 1];
	for(GLint i = 0; i < count; ++i) {
		GLsizei len;
		GLint size;
		GLenum type;
		/* the string that will be returned is NULL terminated */
		gl(GetActiveUniform(program.handle, i, max_len, &len, &size, &type, temp_name));
		uniforms[i].handle = gl(GetUniformLocation(program.handle, temp_name));
		uniforms[i].name = malloc(sizeof(*uniforms[i].name) * (len + 1));
		strcpy(uniforms[i].name, temp_name);
	}
	*uniforms_out = uniforms;
	*size_out = count;

	return 1;
}

int get_attributes(program_t program, attribute_name_t** attributes_out, size_t* size_out) {
	gfx_assert(program.handle != GLHANDLE);

	GLint count, max_len;
	count = get_attribute_count(program);
	gl(GetProgramiv(program.handle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &max_len));
	attribute_name_t* attributes = malloc(sizeof(*attributes) * count);
	if(attributes == NULL) return 0;
	char temp_name[max_len + 1];
	for(GLint i = 0; i < count; ++i) {
		GLsizei len;
		GLint size;
		GLenum type;
		/* the string that will be returned is NULL terminated */
		gl(GetActiveAttrib(program.handle, i, max_len, &len, &size, &type, temp_name));
		attributes[i].handle = gl(GetAttribLocation(program.handle, temp_name));
		attributes[i].name = malloc(sizeof(*attributes[i].name) * (len + 1));
		strcpy(attributes[i].name, temp_name);
	}
	*attributes_out = attributes;
	*size_out = count;

	return 1;
}
