#ifndef GFX_TEXTURE_H
#define GFX_TEXTURE_H

#include "gfx/context.h"

typedef struct {
	GLuint handle;
} texture2d_t;

#define ITEXTURE2D { GLHANDLE }
extern const texture2d_t TEXTURE2D;

inline int active_texture(GLint unit) {
	gl(ActiveTexture(GL_TEXTURE0 + unit));
	return unit;
}

inline void create_texture2d(texture2d_t* texture) {
	gfx_assert(texture->handle == GLHANDLE);
	gl(GenTextures(1, &texture->handle));
}

inline void destroy_texture2d(texture2d_t* texture) {
	gfx_assert(texture->handle != GLHANDLE);
	gl(DeleteTextures(1, &texture->handle));
	*texture = TEXTURE2D;
}

inline void bind_texture2d(texture2d_t texture) {
	gfx_assert(texture.handle != GLHANDLE);
	gl(BindTexture(GL_TEXTURE_2D, texture.handle));
}

inline void unbind_texture2d() {
	gl(BindTexture(GL_TEXTURE_2D, 0));
}

inline texture2d_t bound_texture2d() {
	GLint bound;
	gl(GetIntegerv(GL_TEXTURE_BINDING_2D, &bound));
	texture2d_t texture = TEXTURE2D;
	if(bound > 0) texture.handle = bound;
	return texture;
}

inline void upload_texture2d(GLvoid* data, GLuint width, GLuint height, GLenum type, GLenum format, GLenum internal_format) {
	gfx_assert(bound_texture2d().handle != GLHANDLE);
	gl(TexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, type, data));
}

inline void set_filter_texture2d(GLenum filter_mode) {
	gfx_assert(bound_texture2d().handle != GLHANDLE);
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter_mode));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter_mode));
}

inline void set_wrap_texture2d(GLenum wrap_mode) {
	gfx_assert(bound_texture2d().handle != GLHANDLE);
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_mode));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_mode));
}

#endif
