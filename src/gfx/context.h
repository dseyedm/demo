#ifndef GFX_CONTEXT_H
#define GFX_CONTEXT_H

/* targetting opengl 3.3; glsl 330 */
#include <GL/gl3w.h>

#ifdef gl
#error this macro must not be defined, since we use it as a wrapper around all opengl calls
#else
#include "gfx/assert.h"
#endif

#ifdef NDEBUG
#define gl(OPENGL_CALL) \
	gl##OPENGL_CALL
#else
#define gl(OPENGL_CALL) \
	gl##OPENGL_CALL; \
	gfx_assert(glGetError() == GL_NO_ERROR && #OPENGL_CALL);
#endif

#define GLHANDLE (GLuint)(-1)

/* todo: move the #defines someplace else */
#define SIZE(a) (sizeof(a) / sizeof(a[0]))
#define UNUSED(a) (void)(a)

#endif
