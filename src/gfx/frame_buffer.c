#include "frame_buffer.h"

const fbo_t FBO = IFBO;

extern inline void create_fbo(fbo_t* fbo);
extern inline void destroy_fbo(fbo_t* fbo);
extern inline void bind_fbo(fbo_t fbo);
extern inline void unbind_fbo();
extern inline fbo_t bound_fbo();
extern inline void attach_texture2d_fbo(GLenum attachment, texture2d_t texture2d);
extern inline void detach_texture2d_fbo(GLenum attachment);
extern inline void attach_rbo_fbo(GLenum attachment, rbo_t rbo);
extern inline void detach_rbo_fbo(GLenum attachment);
extern inline GLenum fbo_status();
