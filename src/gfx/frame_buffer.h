#ifndef GFX_FRAME_BUFFER_H
#define GFX_FRAME_BUFFER_H

#include "gfx/context.h"
#include "gfx/texture.h"
#include "gfx/render_buffer.h"

typedef struct {
	GLuint handle;
} fbo_t;

#define IFBO { GLHANDLE }
extern const fbo_t FBO;

inline void create_fbo(fbo_t* fbo) {
	gfx_assert(fbo->handle == GLHANDLE);
	gl(GenFramebuffers(1, &fbo->handle));
}

inline void destroy_fbo(fbo_t* fbo) {
	gfx_assert(fbo->handle != GLHANDLE);
	gl(DeleteFramebuffers(1, &fbo->handle));
	*fbo = FBO;
}

inline void bind_fbo(fbo_t fbo) {
	gfx_assert(fbo.handle != GLHANDLE);
	gl(BindFramebuffer(GL_FRAMEBUFFER, fbo.handle));
}

inline void unbind_fbo() {
	gl(BindFramebuffer(GL_FRAMEBUFFER, 0));
}

inline fbo_t bound_fbo() {
	GLint bound;
	gl(GetIntegerv(GL_FRAMEBUFFER_BINDING, &bound));
	fbo_t fbo = FBO;
	if(bound > 0) fbo.handle = bound;
	return fbo;
}

inline void attach_texture2d_fbo(GLenum attachment, texture2d_t texture2d) {
	/* default fbo is perfectly valid */
	/* gfx_assert(bound_fbo().handle != GLHANDLE); */
	gfx_assert(texture2d.handle != GLHANDLE);
	gl(FramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture2d.handle, 0));
}

inline void detach_texture2d_fbo(GLenum attachment) {
	gl(FramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, 0, 0));
}

inline void attach_rbo_fbo(GLenum attachment, rbo_t rbo) {
	gl(FramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, rbo.handle));
}

inline void detach_rbo_fbo(GLenum attachment) {
	gl(FramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, 0));
}

inline GLenum fbo_status() {
	/* it only makes sense to use this function on the non-default fbo */
	gfx_assert(bound_fbo().handle != GLHANDLE);
	return glCheckFramebufferStatus(GL_FRAMEBUFFER);
}

#endif
