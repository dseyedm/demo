#ifndef GFX_MESH_H
#define GFX_MESH_H

#include "gfx/vertex_array.h"
#include "gfx/vertex_buffer.h"
#include "gfx/shader.h"

#include <linmath/linmath.h>
#include <stdint.h>

typedef struct {
	/* opengl objects */
	vao_t vao;
	vbo_t elements_vbo;
	vbo_t vertices_vbo;

	/* mesh data */
	uint8_t* elements;
	vec2* vertices;

	size_t elements_size, vertices_size;
} mesh2d_t;

#define IMESH2D { IVAO, IVBO, IVBO, NULL, NULL, 0, 0 }
extern const mesh2d_t MESH2D;

void create_mesh2d(mesh2d_t* mesh);
void destroy_mesh2d(mesh2d_t* mesh);

void upload_mesh2d(mesh2d_t* mesh);
/* list order is: vertex; ignore any attributes with handle = (GLuint)(-1) */
void attach_mesh2d(mesh2d_t* mesh, attribute_t* list);

void render_mesh2d_elements(mesh2d_t* mesh, GLenum mode);
void render_mesh2d_vertices(mesh2d_t* mesh, GLenum mode);

typedef struct {
    /* ommitted normals, tangents, bitangents */
    vao_t vao;
    vbo_t elements_vbo;
    vbo_t vertices_vbo;

    uint8_t* elements;
    vec3* vertices;

    size_t elements_size, vertices_size;
} mesh3d_t;

#define IMESH3D { IVAO, IVBO, IVBO, NULL, NULL, 0, 0 }
extern const mesh3d_t MESH3D;

void create_mesh3d(mesh3d_t* mesh);
void destroy_mesh3d(mesh3d_t* mesh);

void upload_mesh3d(mesh3d_t* mesh);
/* list order is: vertex, normal; ignore any attributes with handle = (GLuint)(-1) */
void attach_mesh3d(mesh3d_t* mesh, attribute_t* list);

void render_mesh3d_elements(mesh3d_t* mesh, GLenum mode);
void render_mesh3d_vertices(mesh3d_t* mesh, GLenum mode);

#endif
