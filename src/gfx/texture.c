#include "gfx/texture.h"

const texture2d_t TEXTURE2D = ITEXTURE2D;

extern inline int active_texture(GLint unit);

extern inline void create_texture2d(texture2d_t* texture);
extern inline void destroy_texture2d(texture2d_t* texture);

extern inline void bind_texture2d(texture2d_t texture);
extern inline void unbind_texture2d();
extern inline texture2d_t bound_texture2d();

/* upload_texture2d(&data[0][0][0], X, Y, GL_UNSIGNED_BYTE, GL_RGB, GL_RGB8); */
extern inline void upload_texture2d(GLvoid* data, GLuint width, GLuint height, GLenum type, GLenum format, GLenum internal_format);

extern inline void set_filter_texture2d(GLenum filter_mode);
extern inline void set_wrap_texture2d(GLenum wrap_mode);
