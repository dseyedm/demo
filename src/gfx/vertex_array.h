#ifndef GFX_VERTEX_ARRAY_H
#define GFX_VERTEX_ARRAY_H

#include "gfx/context.h"

typedef struct {
	GLuint handle;
} vao_t;

#define IVAO { GLHANDLE }
extern const vao_t VAO;

inline void create_vao(vao_t* vao) {
	gfx_assert(vao->handle == GLHANDLE);
	gl(GenVertexArrays(1, &vao->handle));
}

inline void destroy_vao(vao_t* vao) {
	gfx_assert(vao->handle != GLHANDLE);
	gl(DeleteVertexArrays(1, &vao->handle));
	*vao = VAO;
}

inline void bind_vao(vao_t vao) {
	gfx_assert(vao.handle != GLHANDLE);
	gl(BindVertexArray(vao.handle));
}

inline void unbind_vao() {
	gl(BindVertexArray(0));
}

inline vao_t bound_vao() {
	GLint bound;
	gl(GetIntegerv(GL_VERTEX_ARRAY_BINDING, &bound));
	vao_t vao = VAO;
	if(bound > 0) vao.handle = bound;
	return vao;
}

#endif
