#ifndef NDEBUG

#include "gfx/assert.h"

#include <stdio.h>
#include <stdlib.h>

void _gfx_assert(const char* condition, const char* file, const char* function, int line) {
	fprintf(stderr, "condition %s failed in function \"%s\" on line %d in %s\n", condition, function, line, file);
	exit(0);
}

#endif
