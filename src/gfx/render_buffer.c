#include "gfx/render_buffer.h"

const rbo_t RBO = IRBO;

extern void create_rbo(rbo_t* rbo);
extern void destroy_rbo(rbo_t* rbo);
extern void bind_rbo(rbo_t rbo);
extern void unbind_rbo();
extern inline rbo_t bound_rbo();
extern void store_rbo(GLenum internal_format, GLsizei width, GLsizei height);
