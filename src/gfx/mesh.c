#include "gfx/mesh.h"
#include "gfx/assert.h"

#include <stdlib.h>

const mesh2d_t MESH2D = IMESH2D;

void create_mesh2d(mesh2d_t* mesh) {
	gfx_assert(mesh->vao.handle == GLHANDLE);

	create_vao(&mesh->vao);
	create_vbo(&mesh->elements_vbo);
	create_vbo(&mesh->vertices_vbo);
}

void destroy_mesh2d(mesh2d_t* mesh) {
	gfx_assert(mesh->vao.handle != GLHANDLE);

	destroy_vao(&mesh->vao);
	destroy_vbo(&mesh->elements_vbo);
	destroy_vbo(&mesh->vertices_vbo);

	*mesh = MESH2D;
}

void upload_mesh2d(mesh2d_t* mesh) {
	gfx_assert(mesh->vao.handle != GLHANDLE);

	bind_vao(mesh->vao);
	if(mesh->elements != NULL) {
		bind_element_vbo(mesh->elements_vbo);
		upload_element_vbo8(mesh->elements, mesh->elements_size);
		unbind_element_vbo();
	}
	if(mesh->vertices != NULL) {
		bind_array_vbo(mesh->vertices_vbo);
		upload_array_vbo2fa(mesh->vertices, mesh->vertices_size);
		unbind_array_vbo();
	}
	unbind_vao();
}

void attach_mesh2d(mesh2d_t* mesh, attribute_t* list) {
	gfx_assert(mesh->vao.handle != GLHANDLE);

	bind_vao(mesh->vao);

	/* vertices */
	if(list[0].handle != GLHANDLE) {
		bind_array_vbo(mesh->vertices_vbo);
		enable_attribute(list[0]);
		set_attribute_ptr(list[0], GL_FLOAT, 2);
		unbind_array_vbo();
	}

	unbind_vao();
}

void render_mesh2d_elements(mesh2d_t* mesh, GLenum mode) {
	gfx_assert(mesh->vao.handle != GLHANDLE);

	bind_vao(mesh->vao);
	bind_element_vbo(mesh->elements_vbo);
	render_element_vbo8(mesh->elements_size, mode);
	unbind_element_vbo();
	unbind_vao();
}

void render_mesh2d_vertices(mesh2d_t* mesh, GLenum mode) {
	gfx_assert(mesh->vao.handle != GLHANDLE);

	bind_vao(mesh->vao);
	bind_array_vbo(mesh->vertices_vbo);
	render_array_vbo(mesh->vertices_size, mode);
	unbind_array_vbo();
	unbind_vao();
}

const mesh3d_t MESH3D = IMESH3D;

void create_mesh3d(mesh3d_t* mesh) {
    gfx_assert(mesh->vao.handle == GLHANDLE);

	create_vao(&mesh->vao);
	create_vbo(&mesh->elements_vbo);
	create_vbo(&mesh->vertices_vbo);
}

void destroy_mesh3d(mesh3d_t* mesh) {
    gfx_assert(mesh->vao.handle != GLHANDLE);

	destroy_vao(&mesh->vao);
	destroy_vbo(&mesh->elements_vbo);
	destroy_vbo(&mesh->vertices_vbo);

	*mesh = MESH3D;
}

void upload_mesh3d(mesh3d_t* mesh) {
    gfx_assert(mesh->vao.handle != GLHANDLE);

	bind_vao(mesh->vao);
	if(mesh->elements != NULL) {
		bind_element_vbo(mesh->elements_vbo);
		upload_element_vbo8(mesh->elements, mesh->elements_size);
		unbind_element_vbo();
	}
	if(mesh->vertices != NULL) {
		bind_array_vbo(mesh->vertices_vbo);
		upload_array_vbo3fa(mesh->vertices, mesh->vertices_size);
		unbind_array_vbo();
	}
	unbind_vao();
}

/* todo: fix this attach method, it's horrible */
void attach_mesh3d(mesh3d_t* mesh, attribute_t* list) {
    gfx_assert(mesh->vao.handle != GLHANDLE);

	bind_vao(mesh->vao);

	/* vertices */
	if(list[0].handle != GLHANDLE) {
		bind_array_vbo(mesh->vertices_vbo);
		enable_attribute(list[0]);
		set_attribute_ptr(list[0], GL_FLOAT, 3);
		unbind_array_vbo();
	}

	unbind_vao();
}

void render_mesh3d_elements(mesh3d_t* mesh, GLenum mode) {
    gfx_assert(mesh->vao.handle != GLHANDLE);

	bind_vao(mesh->vao);
	bind_element_vbo(mesh->elements_vbo);
	render_element_vbo8(mesh->elements_size, mode);
	unbind_element_vbo();
	unbind_vao();
}

void render_mesh3d_vertices(mesh3d_t* mesh, GLenum mode) {
    gfx_assert(mesh->vao.handle != GLHANDLE);

	bind_vao(mesh->vao);
	bind_array_vbo(mesh->vertices_vbo);
	render_array_vbo(mesh->vertices_size, mode);
	unbind_array_vbo();
	unbind_vao();
}
